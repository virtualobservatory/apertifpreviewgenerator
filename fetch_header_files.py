#!/usr/bin/env python
import logging
from argparse import ArgumentParser
import json
import os.path

logging.basicConfig(format='%(asctime)-15s %(name)s %(levelname)-8s: %(message)s')
logger = logging.getLogger('fetch_header_files_from_path_list')


def parse_args():
    parser = ArgumentParser(description='fetch http access reference for a gived dataproduct')
    parser.add_argument('list_of_paths', help='input file')
    parser.add_argument('out_dir', default='previews',
                        help='output directory')

    return parser.parse_args()


def read_path_from_file(file_path):
    with open(file_path, 'r') as f_stream:
        for line in f_stream:
            yield line.rstrip('\n')


def __parse_header_to_dict(header):
    lines = [header[chunk: chunk + 80].strip().rstrip('/') for chunk in range(0, len(header), 80) if
             header[chunk: chunk + 3] != 'END']
    comments = "\n".join(filter(lambda x: x.startswith('COMMENT'), lines))
    history = "\n".join(filter(lambda x: x.startswith('HISTORY'), lines))

    key_value_list = [line.split('=') for line in lines if
                      not line.startswith('COMMENT') and not line.startswith('HISTORY')]
    result = {'COMMENTS': comments, 'HISTORY': history}
    for pair in key_value_list:
        try:
            key, value = pair if len(pair) > 1 else (pair, 'UNDEFINED')
            key, value = key.strip(), value.strip()
            if key not in ['SIMPLE', 'EXTEND']:
                if '\'' in value:
                    value = value.strip('\'').strip()
                elif '/' in value:
                    value = value.split('/')[0]
                else:
                    value = float(value)
        except Exception as e:
            raise Exception('cannot parse %s' % pair)
        if key in result:
            result[key] += [value] if isinstance(result[key], list) else [result[key], value]
        else:
            result[key] = value
    return result


def store_record_to_file_tagged_by_path(output_directory, record, path):
    os.makedirs(output_directory, exist_ok=True)
    path_to_file_out = os.path.join(output_directory, 'HEADER-{}.json'.format(path.replace('/', '.')))

    with open(path_to_file_out, 'w') as fstream:
        json.dump(record, fstream)


def make_url_from_path(path,
                       base_url='https://alta.astron.nl/webdav/APERTIF_DR1_Imaging',
                       base_path='/irodsdata/ALTA/archive/apertif_main/visibilities_default/'):
    trimmed_path = path.replace(base_path, '')
    url = f'{base_url}/{trimmed_path}'
    return url


def open_file_and_get_header(path):
    if not os.path.exists(path):
        raise FileNotFoundError(path)
    file_size = os.path.getsize(path)

    header_content = ''
    with open(path, 'rb') as f_stream:
        while True:
            data = f_stream.read(80)
            header_content += (data.decode())
            if 'END         ' in header_content:
                break
    result = __parse_header_to_dict(header_content)
    result['file_size'] = file_size

    result['url'] = make_url_from_path(path)
    return result


def main():
    args = parse_args()
    for path in read_path_from_file(args.list_of_paths):
        try:
            header = open_file_and_get_header(path)
            path = header['url']
            store_record_to_file_tagged_by_path(args.out_dir, header, path)
            print('processed path', path)
        except Exception as e:
            logger.exception(e)
            print('cannot process ', path, 'skipping...')


if __name__ == '__main__':
    main()
