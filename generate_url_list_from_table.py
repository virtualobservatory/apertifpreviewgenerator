#!/usr/bin/env python
import logging
from argparse import ArgumentParser
import pandas
import subprocess
import os.path

logging.basicConfig(format='%(asctime)-15s %(name)s %(levelname)-8s: %(message)s')
logger = logging.getLogger('generate_preview_from_url_list')


def parse_args():
    parser = ArgumentParser(description='fetch http access reference for a gived dataproduct')
    parser.add_argument('file_in', help='input file')
    parser.add_argument('--file_name', help='grep file name', default='image_mf_02.fits')
    parser.add_argument('out_dir', default='previews',
                        help='output directory')

    return parser.parse_args()


def get_list_of_obsid_from_file(file_in):
    file_in = pandas.read_csv(file_in)

    obs_ids = file_in['ObsID'].to_numpy()
    beams = file_in['Beam'].to_numpy()
    return obs_ids, beams


def location_files_from_obs_id(obs_id, beam):
    command = 'iquest "%s,%s" "SELECT RESC_LOC, DATA_PATH WHERE COLL_NAME' \
              ' like \'/altaZone/archive/apertif_main/visibilities_default/{obs_id}_AP_B{beam:03d}\'' \
              ' AND DATA_NAME LIKE \'%.fits\'"'.format(obs_id=obs_id, beam=beam)
    output = filter(lambda x: x != '', subprocess.check_output(command, shell=True).decode().split('\n'))
    loc_local_path = list(map(lambda x: x.split(','), output))
    return loc_local_path


def main():
    args = parse_args()
    obs_ids, beams = get_list_of_obsid_from_file(args.file_in)
    for obs, beam in zip(obs_ids, beams):
        logger.info('processing %s/%s', obs, beam)
        list_of_files = location_files_from_obs_id(obs, beam)

        for site, path in list_of_files:
            file_path = os.path.join(args.out_dir, '{}.txt'.format(site))
            with open(file_path, 'a+') as f_out:
                f_out.write('{}\n'.format(path))


if __name__ == '__main__':
    main()
