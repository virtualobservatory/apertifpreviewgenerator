#!/usr/bin/env python
from argparse import ArgumentParser
import logging
import astropy.io.fits as fits
import os
import gc
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy.nddata import Cutout2D
from astropy import units as u
import matplotlib.pyplot as plt
import numpy

logging.basicConfig(format='%(asctime)-15s %(name)s %(levelname)-8s: %(message)s')
logger = logging.getLogger('generate_preview_from_url_list')


def parse_args():
    parser = ArgumentParser(description='fetch http access reference for a gived dataproduct')
    parser.add_argument('file_in', help='input file')

    parser.add_argument('out_dir', default='previews',
                        help='output directory')
    parser.add_argument('--cache', action='store_true',
                        help='cache downloaded fits')

    return parser.parse_args()


def read_url_list(path):
    with open(path, 'r') as f_stream:
        return [line.rstrip('\n') for line in f_stream.readlines()]


def relative_path_from_url(url):
    return os.path.sep.join(url.split('/')[-3:-1])


def prepare_output_dir_for_url(url, base_path):
    relative_path = relative_path_from_url(url)
    absolute_path = os.path.join(base_path, relative_path)

    logger.debug('creating path %s if not exist', absolute_path)
    make_output_dir_if_doesnt_exist(absolute_path)
    logger.debug('path %s present/created', absolute_path)
    return absolute_path


def output_file_from_url(url):
    return url.split('/')[-1].rstrip('.fits') + '.png'


def _remove_empty_axes_in_continuum_image(img, wcs):
    if wcs.naxis == 4:
        wcs = wcs.dropaxis(3)
        img = img[0]
    if wcs.naxis == 3:
        wcs = wcs.dropaxis(2)
        img = img[0]

    return img, wcs


def plot_continuus_image(hdu: fits.ImageHDU, path: str, file_name: str,
                         dpi=70, min_value=-.2, max_value=1):
    out_file_path = os.path.join(path, file_name)
    header = hdu.header
    wcs = WCS(header)
    position = SkyCoord(header['CRVAL1'], header['CRVAL2'], unit='deg')
    img, wcs = _remove_empty_axes_in_continuum_image(hdu.data, wcs)

    size = u.Quantity((1.5, 1.5), u.degree)

    cutout = Cutout2D(img, position, size, wcs=wcs)

    # set up plot
    fig, ax = plt.subplots(figsize=[12.36, 10.45])
    ax = plt.subplot(projection=cutout.wcs)
    pl = ax.imshow(cutout.data * 1.e3, vmin=min_value, vmax=max_value,
                   origin='lower', cmap="viridis", alpha=1)

    ax.set_title('Continuum image')

    ax.coords[0].set_axislabel('Right Ascension')
    ax.coords[1].set_axislabel('Declination')
    ax.coords[0].set_major_formatter('hh:mm')

    ax.tick_params(axis="both", which="both", direction="in")

    ax.coords[0].display_minor_ticks(True)
    ax.coords[0].set_minor_frequency(5)
    ax.coords[1].display_minor_ticks(True)
    ax.coords[1].set_minor_frequency(5)
    fig.savefig(out_file_path, overwrite=True, bbox_inches='tight', dpi=dpi)
    logger.info('Created plot file in: %s', out_file_path)
    plt.close(fig)


def plot_cubes_image(hdu: fits.ImageHDU, path: str, file_name: str,
                     dpi=70, min_value=-.2, max_value=1):
    out_file_path = os.path.join(path, file_name)
    wcs = WCS(hdu.header)
    data = numpy.array(hdu.data)
    n_channels = data.shape[0]
    data[numpy.isnan(data)] = 0.
    _, wcs = _remove_empty_axes_in_continuum_image(data, wcs)

    central_channel = n_channels//2

    out_data = numpy.mean(data[central_channel - 5: central_channel + 5], axis=0)

    # set up plot
    fig, ax = plt.subplots(figsize=[12.36, 12.00])
    ax = plt.subplot(projection=wcs)
    ax.imshow(out_data,
              origin='lower', cmap="viridis", alpha=1)
    ax.set_title('Spectral cube')

    ax.coords[0].set_axislabel('Right Ascension')
    ax.coords[1].set_axislabel('Declination')
    ax.coords[0].set_major_formatter('hh:mm')

    ax.tick_params(axis="both", which="both", direction="in")

    ax.coords[0].display_minor_ticks(True)
    ax.coords[0].set_minor_frequency(5)
    ax.coords[1].display_minor_ticks(True)
    ax.coords[1].set_minor_frequency(5)
    fig.savefig(out_file_path, overwrite=True, bbox_inches='tight', dpi=dpi)
    logger.info('Created plot file in: %s', out_file_path)
    plt.close(fig)


def plot_hdu_into(hdu, path, file_name):
    if 'image_mf' in file_name:
        plot_continuus_image(hdu, path, file_name)
    if 'cube' in file_name:
        plot_cubes_image(hdu, path, file_name)


def generate_preview_for_url(url, base_path, cache=False, skip_existing=True):
    out_path = prepare_output_dir_for_url(url, base_path)
    out_filename = output_file_from_url(url)
    if skip_existing and os.path.exists(os.path.join(out_path, out_filename)):
        logger.info('skipping generation of existing file %s%s%s', out_path, os.path.sep, out_filename)
        return
    try:
        with fits.open(url, stream=True, cache=cache, lazy_load_hdus=True) as fits_file:
            plot_hdu_into(fits_file[0], out_path, out_filename)
    except FileNotFoundError:
        logger.info('skip missing file %s', url)
    gc.collect()


def generate_previews_from_urls(urls, base_path, cache=False):
    for url in urls:
        generate_preview_for_url(url, base_path, cache=cache)


def make_output_dir_if_doesnt_exist(output):
    os.makedirs(output, exist_ok=True)


def main():
    args = parse_args()
    urls = read_url_list(args.file_in)
    make_output_dir_if_doesnt_exist(args.out_dir)
    generate_previews_from_urls(urls, args.out_dir, cache=args.cache)


if __name__ == '__main__':
    logger.setLevel(logging.DEBUG)
    main()
