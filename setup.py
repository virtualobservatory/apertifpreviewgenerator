import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="apertifpreviewgenerator",
    version="0.0.8",
    author="Mattia Mancini",
    author_email="mancini@astron.nl",
    description="A simple script to generate previews from the APERTIF data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.astron.nl/virtualobservatory/apertifpreviewgenerator.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache 2 License",
        "Operating System :: OS Independent",
    ],
    scripts=['generate_previews_from_url_list.py',
             'generate_url_list_from_table.py',
             'fetch_header_files.py'],
    python_requires='>=3.6',
    install_requires=[
        'numpy',
        'astropy',
        'matplotlib',
        'pandas'
    ],
)
